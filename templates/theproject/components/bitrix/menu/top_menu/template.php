<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<header class="header
<?
if (($arParams['FIXED_MENU']) == 'Y') echo ' fixed' ?>
<? if (($arParams['DARK_MENU']) == 'Y') echo ' dark' ?>
<? if (($arParams['FULL_WIDTH_MENU']) == 'Y') echo ' full-width' ?>
<? if (($arParams['CENTRED_MENU']) == 'Y') echo ' centered' ?>
 clearfix">

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- header-left start -->
                <!-- ================ -->
                <div class="header-left clearfix">
                    <div id="logo" class="logo">
                        <a href="index.html"><img id="logo_img"
                                                  src="<?= SITE_TEMPLATE_PATH ?>/images/logo_light_blue.png"
                                                  alt="The Project"></a>
                    </div>
                    <div class="site-slogan">
                        Multipurpose HTML5 Template
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <? if (!empty($arResult)): ?>
                <div class="header-right clearfix">
                    <div class="main-navigation  animated with-dropdown-buttons">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                        <?
                                        $previousLevel = 0;
                                        foreach ($arResult as $arItem): ?>

                                        <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
                                            <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
                                        <? endif ?>

                                        <? if ($arItem["IS_PARENT"]): ?>

                                        <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                                        <li class="dropdown"><a href="<?= $arItem["LINK"] ?>" class="dropdown-toggle"
                                                                data-toggle="dropdown"><?= $arItem["TEXT"] ?></a>
                                            <ul class="dropdown-menu">
                                                <? else: ?>
                                                <li<? if ($arItem["SELECTED"]): ?> class="active dropdown"<? endif ?>><a
                                                        href="<?= $arItem["LINK"] ?>" class="dropdown-toggle"
                                                        data-toggle="dropdown"><?= $arItem["TEXT"] ?></a>
                                                    <ul class="dropdown-menu">
                                                        <? endif ?>

                                                        <? else: ?>

                                                            <? if ($arItem["PERMISSION"] > "D"): ?>

                                                                <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                                                                    <li><a href="<?= $arItem["LINK"] ?>"
                                                                           class="<? if ($arItem["SELECTED"]): ?>root-active<? else: ?>root-item<? endif ?>"><?= $arItem["TEXT"] ?></a>
                                                                    </li>
                                                                <? else: ?>
                                                                    <li<? if ($arItem["SELECTED"]): ?> class="active"<? endif ?>>
                                                                        <a
                                                                            href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                                                                    </li>
                                                                <? endif ?>

                                                            <? else: ?>

                                                                <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                                                                    <li><a href=""
                                                                           class="<? if ($arItem["SELECTED"]): ?>active<? else: ?>root-item<? endif ?>"
                                                                           title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                                                    </li>
                                                                <? else: ?>
                                                                    <li><a href="" class="denied"
                                                                           title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                                                    </li>
                                                                <? endif ?>
                                                            <? endif ?>
                                                        <? endif ?>
                                                        <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>
                                                        <? endforeach ?>
                                                        <? if ($previousLevel > 1)://close last item tags?>
                                                            <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
                                                        <? endif ?>
                                                    </ul>
                                                    <div class="menu-clear-left"></div>
                                                    <? endif ?>
                                                    <div class="header-dropdown-buttons hidden-xs ">
                                                        <div class="btn-group dropdown">
                                                            <button type="button" class="btn dropdown-toggle"
                                                                    data-toggle="dropdown"><i
                                                                    class="icon-search"></i></button>
                                                            <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                                                                <li>
                                                                    <form role="search" class="search-box margin-clear">
                                                                        <div class="form-group has-feedback">
                                                                            <input type="text" class="form-control"
                                                                                   placeholder="Search">
                                                                            <i class="icon-search form-control-feedback"></i>
                                                                        </div>
                                                                    </form>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
