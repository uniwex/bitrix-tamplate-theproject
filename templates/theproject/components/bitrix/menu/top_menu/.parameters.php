<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(

	"FIXED_MENU" => Array(
		"NAME" => GetMessage("FIXED_MENU_DESC"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DARK_MENU" => Array(
		"NAME" => GetMessage("DARK_MENU_DESC"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"FULL_WIDTH_MENU" => Array(
		"NAME" => GetMessage("FULL_WIDTH_MENU_DESC"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"CENTRED_MENU" => Array(
		"NAME" => GetMessage("CENTRED_MENU_DESC"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
);



?>