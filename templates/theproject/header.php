<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta charset="utf-8">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Fontello CSS -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH ?>/plugins/rs-plugin/css/settings.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/animations.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH ?>/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH ?>/plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH ?>/plugins/hover/hover-min.css" rel="stylesheet">

    <!-- the project core CSS file -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/style.css" rel="stylesheet">

    <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/skins/light_blue.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/custom.css" rel="stylesheet">
</head>

<!-- body classes:  -->
<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
<!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
<body class="no-trans front-page transparent-header  ">
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?>
</div>
<!-- scrollToTop -->
<!-- ================ -->
<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

<!-- page wrapper start -->
<!-- ================ -->
<div class="page-wrapper">

    <!-- header-container start -->
    <div class="header-container">

        <!-- header-top start -->
        <!-- classes:  -->
        <!-- "dark": dark version of header top e.g. class="header-top dark" -->
        <!-- "colored": colored version of header top e.g. class="header-top colored" -->
        <!-- ================ -->
        <div class="header-top dark ">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 col-sm-6 col-md-9">
                        <!-- header-top-first start -->
                        <!-- ================ -->
                        <div class="header-top-first clearfix">
                            <ul class="social-links circle small clearfix hidden-xs">
                                <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i
                                            class="fa fa-twitter"></i></a></li>
                                <li class="skype"><a target="_blank" href="http://www.skype.com"><i
                                            class="fa fa-skype"></i></a></li>
                                <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i
                                            class="fa fa-linkedin"></i></a></li>
                                <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i
                                            class="fa fa-google-plus"></i></a></li>
                                <li class="youtube"><a target="_blank" href="http://www.youtube.com"><i
                                            class="fa fa-youtube-play"></i></a></li>
                                <li class="flickr"><a target="_blank" href="http://www.flickr.com"><i
                                            class="fa fa-flickr"></i></a></li>
                                <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i
                                            class="fa fa-facebook"></i></a></li>
                                <li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i
                                            class="fa fa-pinterest"></i></a></li>
                            </ul>
                            <div class="social-links hidden-lg hidden-md hidden-sm circle small">
                                <div class="btn-group dropdown">
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i
                                            class="fa fa-share-alt"></i></button>
                                    <ul class="dropdown-menu dropdown-animation">
                                        <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i
                                                    class="fa fa-twitter"></i></a></li>
                                        <li class="skype"><a target="_blank" href="http://www.skype.com"><i
                                                    class="fa fa-skype"></i></a></li>
                                        <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i
                                                    class="fa fa-linkedin"></i></a></li>
                                        <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i
                                                    class="fa fa-google-plus"></i></a></li>
                                        <li class="youtube"><a target="_blank" href="http://www.youtube.com"><i
                                                    class="fa fa-youtube-play"></i></a></li>
                                        <li class="flickr"><a target="_blank" href="http://www.flickr.com"><i
                                                    class="fa fa-flickr"></i></a></li>
                                        <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i
                                                    class="fa fa-facebook"></i></a></li>
                                        <li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i
                                                    class="fa fa-pinterest"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="list-inline hidden-sm hidden-xs">
                                <li><i class="fa fa-map-marker pr-5 pl-10"></i>One Infinity Loop Av, Tk 123456</li>
                                <li><i class="fa fa-phone pr-5 pl-10"></i>+12 123 123 123</li>
                                <li><i class="fa fa-envelope-o pr-5 pl-10"></i> theproject@mail.com</li>
                            </ul>
                        </div>
                        <!-- header-top-first end -->
                    </div>
                    <div class="col-xs-9 col-sm-6 col-md-3">

                        <!-- header-top-second start -->
                        <!-- ================ -->
                        <div id="header-top-second" class="clearfix">

                            <!-- header top dropdowns start -->
                            <!-- ================ -->
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group">
                                    <a href="<?= SITE_TEMPLATE_PATH ?>/page-signup.html" class="btn btn-default btn-sm"><i
                                            class="fa fa-user pr-10"></i> Sign Up</a>
                                </div>
                                <div class="btn-group dropdown">
                                    <button type="button" class="btn dropdown-toggle btn-default btn-sm"
                                            data-toggle="dropdown"><i class="fa fa-lock pr-10"></i> Login
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                                        <li>
                                            <form class="login-form margin-clear">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">Username</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                    <i class="fa fa-user form-control-feedback"></i>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">Password</label>
                                                    <input type="password" class="form-control" placeholder="">
                                                    <i class="fa fa-lock form-control-feedback"></i>
                                                </div>
                                                <button type="submit" class="btn btn-gray btn-sm">Log In</button>
                                                <span class="pl-5 pr-5">or</span>
                                                <button type="submit" class="btn btn-default btn-sm">Sing Up</button>
                                                <ul>
                                                    <li><a href="#">Forgot your password?</a></li>
                                                </ul>
                                                <span class="text-center">Login with</span>
                                                <ul class="social-links circle small colored clearfix">
                                                    <li class="facebook"><a target="_blank"
                                                                            href="http://www.facebook.com"><i
                                                                class="fa fa-facebook"></i></a></li>
                                                    <li class="twitter"><a target="_blank"
                                                                           href="http://www.twitter.com"><i
                                                                class="fa fa-twitter"></i></a></li>
                                                    <li class="googleplus"><a target="_blank"
                                                                              href="http://plus.google.com"><i
                                                                class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--  header top dropdowns end -->
                        </div>
                        <!-- header-top-second end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- header-top end -->

        <!-- header start -->
        <!-- classes:  -->
        <!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
        <!-- "dark": dark version of header e.g. class="header dark clearfix" -->
        <!-- "full-width": mandatory class for the full-width menu layout -->
        <!-- "centered": mandatory class for the centered logo layout -->
        <!-- ================ -->

                        <!-- header-right start -->
                        <!-- ================ -->
                        <? $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "top_menu",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"FIXED_MENU" => "N",
		"DARK_MENU" => "Y",
		"FULL_WIDTH_MENU" => "Y",
		"CENTRED_MENU" => "N"
	),
	false
); ?>
                        <!-- header-right end -->

    </div>
    <div id="page-start"></div>